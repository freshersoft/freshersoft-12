#!/bin/sh


git branch apps
git checkout apps
# Add changes to git.
git add .

# Commit changes.
msg="rebuilding site $(date)"
if [ -n "$*" ]; then
	msg="$*"
fi
git commit -m "$msg"

git checkout master 
git merge apps
git branch -d apps


# Push source and build repos.
git push origin master